"use strict";

function isValidJSON(str) {
  try {
    JSON.parse(str);
  } catch (x) {
    return false;
  }
  return true;
}

console.log(isValidJSON('{"a": 2}')); // result: true;
console.log(isValidJSON('{"a: 2')); // result: false;
