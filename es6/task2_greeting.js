"use strict";

function greeting({ name, surname, age }) {
  return `Hello, my name is ${name} ${surname}  and I am ${age} years old`;
}

console.log(greeting({ name: "Bill", surname: "Jacobson", age: 33 }));
console.log(greeting({ name: "Jim", surname: "Power", age: 11 }));
