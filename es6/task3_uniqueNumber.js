"use strict";
// result: [1, 2, 3, 5, 4]

function unique(arr) {
  let newSet = new Set();

  for (let i = 0; i < arr.length; i++) {
    newSet.add(arr[i]);
  }

  return [...newSet];
}

console.log(unique([1, 1, 2, 3, 5, 4, 5]));
